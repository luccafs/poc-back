﻿using Microsoft.AspNetCore.SignalR;
using poc_back.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace poc_back.Service.HubConfig
{
    public class ChartHub : Hub
    {
        public async Task BroadcastChartData(List<ChartModel> data) => 
            await Clients.All.SendAsync("broadcastchartdata", data);
    }

}

