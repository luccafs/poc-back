﻿using Dapper;
using Microsoft.Extensions.Configuration;
using poc_back.Domain.Models;
using poc_back.Domain.Interfaces;

namespace poc_back.Infrastructure.Repositories
{
    public class UserRepository : SqlServerRepository, IUserRepository
    {

        public UserRepository(IConfiguration config) : base(config)
        {
        }

        #region 'New User'
        public User Create(UserDto newUser, byte[] passwordSalt)
        {
            var user = new User() {
                DsName = newUser.DsName,
                DsPassword = newUser.DsPassword,
                DsEmail = newUser.DsEmail,
                DsSalt = Convert.ToBase64String(passwordSalt)
            };
            var insert = "INSERT INTO [USER] (DsName, DsPassword, DsEmail, DsSalt)" +
                "VALUES (@DsName, @DsPassword, @DsEmail, @DsSalt)";
            try
            {
                
                var userId = Database.ExecuteScalar<int>(insert, user);
                user.IdUser = userId;
            }
            catch(Exception e)
            {
                throw new ApplicationException(e.Message);
            }
            return user;
        }
        #endregion

        #region 'Get User'
        public User getUser(string email)
        {
            var parameters = new { email = email };
            string sql = "SELECT * FROM [User] WHERE DsEmail = @email";

            var user = Database.QuerySingleOrDefault<User>(sql, parameters);
            return user;
        }
        #endregion

    }
}
