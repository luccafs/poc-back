﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace poc_back.Domain.Models
{
    public class LoginModel
    {
        public string DsEmail { get; set; }
        public string DsPassword { get; set; }
    }
}
